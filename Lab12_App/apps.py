from django.apps import AppConfig


class Lab12AppConfig(AppConfig):
    name = 'Lab12_App'
