from django.db import models


# Create your models here.
class Account(models.Model):
    email = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    password = models.CharField(max_length=255)


class Gift(models.Model):
    account = models.ForeignKey(Account, models.CASCADE)
    url = models.CharField(max_length=512)
