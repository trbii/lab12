from django.http import HttpResponse
from django.views import View
from django.shortcuts import render, redirect
from Lab12_App.models import Account

# Create your views here.


def getActiveUser(request):
    user = request.session.get('user')
    if request.session.get('user') is not None:
        try:
            user = Account.objects.get(id=user)
        except Account.DoesNotExist:
            user = None
    return user

class Root(View):
    def get(self, request):
        user = getActiveUser(request)
        context = {'user': user}
        if user is not None:
            context['user_list'] = Account.objects.all()
        return render(request, 'index.html', context)

    def post(self, request):
        if request.session.get("user"):
            return redirect('login')
        username = request.POST.get("username")
        password = request.POST.get("password")
        try:
            account = Account.objects.get(name=username, password=password)
            if account.password == password:
                request.session['user'] = account.id
                return self.get(request)
        except Account.DoesNotExist:
            return render(request, 'index.html', {'response': "Account does not exist, please <a href='/register'>register</a>.", 'user': getActiveUser(request)})


class RegisterView(View):
    def get(self, request):
        return render(request, 'register.html', {'user': getActiveUser(request)})

    def post(self, request):
        username = request.POST.get("username")
        password = request.POST.get("password")
        email = request.POST.get("email")

        name_query = Account.objects.filter(name=username)
        email_query = Account.objects.filter(email=email)
        if name_query.count() != 0 or email_query.count() != 0:
            return render(request, "register.html", {'old': request.POST, 'message': "Username or email already exists"})
        if len(password) == 0:
            return render(request, "register.html",
                          {'user': getActiveUser(request), 'old': request.POST, 'message': "A valid password is needed"})

        account = Account.objects.create(name=username, password=password, email=email)
        return HttpResponse("success!")


class LogoutView(View):
    def get(self, request):
        request.session.flush()
        return redirect('login')


class GiftView(View):
    def get(self, request):
        pass


class UsersView(View):
    def get(self, request):
        pass

