"""Lab12_Project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Lab12_App.views import Root, LogoutView, RegisterView, GiftView, UsersView

urlpatterns = [
    path('', Root.as_view(), name='login'), #login page
    path('logout', LogoutView.as_view(), name='logout'), #logout page
    path('register', RegisterView.as_view(), name='register'), #register page
    path('gifts', GiftView.as_view(), name='gifts'), #gifts page
    path('users', UsersView.as_view(), name='users') #user's giftlist page

]
